export type LinkProps = {
    angle: number,
    color: string,
    href: string,
    title: string,
};

export default LinkProps;